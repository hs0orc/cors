from flask import Flask, jsonify, request, abort
from models import setup_db, Plant

def create_app(test_config=None):
    app = Flask(__name__)
    setup_db(app)

    # @app.route('/')
    # def hello():
    #     return jsonify({'message': 'HELLO WORLD'})

    @app.route('/test', methods=['GET', 'POST'])
    def test():
        plant = Plant('plant_china123', 'science_name', True, 'blue')
        #print(plant)
        plant.insert()
        return 'ok'
        # return plant

        # plant.name =


    # @app.route('/plants, methods=['GET', 'POST'])
    @app.route('/plants', methods=['GET', 'POST'])
    def get_plants():
        page = request.args.get('page', 1, type=int) ## QUERY PARAMETERS
        start = (page - 1) * 10
        end = start + 10
        plants = Plant.query.all()
        formatted_plants = [plant.format() for plant in plants] ## QUERY PARAMETERS

        return jsonify({
            'success': True,
            'plants': formatted_plants[start:end], ## QUERY PARAMETERS
            'total_plants': len(formatted_plants)
        })

    @app.route('/plants/<int:plant_id>')
    def get_specific_plant(plant_id):
        plant = Plant.query.filter(Plant.id == plant_id).one_or_none()

        if plant is None:
            abort(404)
        else:
            return jsonify({
            'success': True,
            'plant': plant.format()
            })


    return app